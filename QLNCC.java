/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package newpro;
import java.io.*;
import java.text.*;
import java.util.*;
/**
 *
 * @author My Computer
 */
public class QLNCC {
    ArrayList<NhaCungCap> DSach ;
            public QLNCC(){
                try{DSach=readds();
                }catch(Exception ex)
                {
                    System.out.println(ex.getMessage());
                }
            }
    public Scanner sc = new Scanner(System.in);

    public ArrayList<NhaCungCap> readds() throws ParseException {
        BufferedReader br = null;
        DSach = new ArrayList<>();
       try {
            File file=new File("D:\\DoAn\\nhacungcap.txt");
            if(!file.exists()){file.createNewFile();}
            br = new BufferedReader(new FileReader("D:\\DoAn\\nhacungcap.txt"));
            String ncc = br.readLine();
            while (ncc != null) {
                NhaCungCap ds = new NhaCungCap(ncc);
                DSach.add(ds);
                ncc = br.readLine();
                br.close();
            }
        } catch (IOException e) {System.out.println(Arrays.toString(e.getStackTrace()));
}
        return DSach;
    }
    

    public void Ghitep() throws IOException {
        BufferedWriter bw = null;
        try {
            bw = new BufferedWriter(new FileWriter("D:\\DoAn\\nhacungcap.txt"));
            for (int i = 0; i < DSach.size(); i++) {
                bw.write(DSach.get(i).toString());
                bw.newLine();
            }
        } catch (IOException e) {
        } finally {
            try {
                bw.close();
                System.out.println("Ghi tep thanh cong");
            } catch (IOException e) {
            }
        }
    }

    public void nhapds() throws ParseException {
        do {
            NhaCungCap ds = new NhaCungCap();
            ds.Nhap(DSach);
            DSach.add(ds);
            System.out.println("Nhan phim bat ky de tiep tuc hoac enter de ket thuc ! ");
            String kt = sc.nextLine();
            if (kt.equals("")) {
                break;
            }
        } while (true);
    }

    public ArrayList<NhaCungCap> TKTenncc( String ten) throws ParseException {
        ArrayList<NhaCungCap> ds = new ArrayList<>();
        for (int i = 0; i < DSach.size(); i++) {
            if (DSach.get(i).getTenncc().toLowerCase().contains(ten.toLowerCase())) {
                ds.add(DSach.get(i));
            }
        }
        return ds;
    }

    public NhaCungCap TKMancc( String Ma) {
        for (int i = 0; i < DSach.size(); i++) {
            if (DSach.get(i).getMancc().contains(Ma)) {
                return DSach.get(i);
            }
        }
        return null;
    }

    public ArrayList<NhaCungCap> TKDCncc( String dc) throws ParseException {
        ArrayList<NhaCungCap> ds = new ArrayList<>();
        for (int i = 0; i < DSach.size(); i++) {
            if (DSach.get(i).getDC().toLowerCase().contains(dc.toLowerCase())) {
                ds.add(DSach.get(i));
            }
        }
        return ds;
    }
    public void Capnhat() throws ParseException {
        int cs = -1;
        System.out.println("Nhap ma nha cung cap can sua : ");
        String mancc = sc.nextLine();
        for (int i = 0; i < DSach.size(); i++) {
            if (DSach.get(i).getMancc().contains(mancc)) { //cái contains là phương thức trả về đúng hoặc sai khi mà cái xâu mình cần ktra nó chứa xâu trong phần () ý
                cs = i;
            }
        }
        if (cs != -1) {
            DSach.get(cs).update();
        } else {
            System.out.println("Khong ton tai ma nha cung cap nay ! ");
        }
    }
    public void Xoa() {

        int cs = -1;
        System.out.println("Nhap ma nha cung cap can xoa : ");
        String mancc = sc.nextLine();
        for (int i = 0; i < DSach.size(); i++) {
            if (DSach.get(i).getMancc().contains(mancc)) {
                cs = i;
            }
        }
        if (cs != -1) {
            DSach.remove(cs);
            System.out.println("Xoa thanh cong!");
        } else {
            System.out.println("Khong ton tai ma nhan vien nay ! ");
        }
    }

    public void hienthids(ArrayList<NhaCungCap> dsncc) {
        System.out.println("Danh sach thong tin nhan vien :");
        System.out.printf("\n%-10s\t%-20s\t%-10s\t%-30s\t%-9s\n", "Ma nha cung cap", "Ten nha cung cap", "Dien thoai", "Dia chi", "CMND");
        for (int i = 0; i < dsncc.size(); i++) {
            dsncc.get(i).Hienthi();
        }
    }

    public void menusearch() throws ParseException, IOException {
        Scanner scr = new Scanner(System.in);
        QLNCC chose = new QLNCC();
        String chon;
        do {
            System.out.println("\n");
            System.out.println("------------------------------");
            System.out.println(" Chon phuong thuc tim kiem! ");
            System.out.println(" 1.Tim kiem theo ma nha cung cap ");
            System.out.println(" 2.Tim kiem theo ten nha cung cap");
            System.out.println(" 3.Tim kiem theo dia chi nha cung cap");
            System.out.println(" 0.Exit ");
            chon = scr.nextLine();
            switch (chon) {
                case "1":
                    System.out.println("Nhap ma nha cung cap can tim ");
                    String Ma = scr.nextLine();
                    NhaCungCap dsm = chose.TKMancc( Ma);
                    if (dsm == null) {
                        System.out.println("Nha cung cap nay khong ton tai! ");
                    } else {
                        dsm.Hienthi();
                    }
                    break;
                case "2":
                    System.out.println("Nhap ten nha cung cap can tim ");
                    String ten = scr.nextLine();
                    ArrayList<NhaCungCap> dsncc = chose.TKTenncc( ten);
                    if (dsncc.isEmpty()) {
                        System.out.println("Nha cung cap nay khong ton tai! ");
                    } else {
                        chose.hienthids(dsncc);
                    }
                    break;
                    case "3":
                    System.out.println("Nhap dia chi nha cung cap can tim ");
                    String dchi = scr.nextLine();
                    ArrayList<NhaCungCap> ds = chose.TKDCncc( dchi);
                    if (ds.isEmpty()) {
                        System.out.println("Nha cung cap nay khong ton tai! ");
                    } else {
                        chose.hienthids(ds);
                    }
                    break;

                case "0":
                        menu n=new menu();n.menuchinh();
                    //chose.menudk1();
                    break;
            }
        } while (true);
    }

    public void menudk1() throws ParseException, IOException {
        Scanner scr = new Scanner(System.in);
        QLNCC chose = new QLNCC();
        String chon;
        boolean kt;
        do {
            chon = null;
            kt = true;
            System.out.println("\n");
            System.out.println("QUAN LY DANH SACH NHA CUNG CAP");
            System.out.println("------------------------------");
            System.out.println(" 1.Nhap danh sach nha cung cap ");
            System.out.println(" 2.Hien thi danh sach nha cung cap ");
            System.out.println(" 3.Cap nhat danh sach nha cung cap ");
            System.out.println(" 4.Xoa danh sach nh cung cap ");
            System.out.println(" 5.Tim kiem ");
            System.out.println(" 0.Exit ");
            System.out.println("------------------------------");
            do {
                try {
                    System.out.println(" Chon cong viec can thuc hien ! ");
                    chon = scr.nextLine();
                    if (chon.compareTo("0") == -1 || chon.compareTo("5") == 1) {
                        throw new Exception("");
                    }
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            } while (chon.compareTo("0") == -1 || chon.compareTo("5") == 1);

            switch (chon) {
                case "1":
                    nhapds();
                    Ghitep();
                    break;
                case "2":
                    hienthids(DSach);
                    break;
                case "3":
                    Capnhat();
                    Ghitep();
                    break;
                case "4":
                    Xoa();
                    break;
                case "5":
                    menusearch();
                    break;
                case "0":
                    kt = false;
                    break;
            }
        } while (kt);
    }
}
