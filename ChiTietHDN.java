/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package newpro;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author My Computer
 */
public class ChiTietHDN {
    public String MaG;
    public int SL;
    public int DG;

    /**
     * @return the MaG
     */
    public String getMaG() {
        return MaG;
    }

    /**
     * @param MaG the MaG to set
     */
    public void setMaG(String MaG) {
        this.MaG = MaG;
    }

    /**
     * @return the SL
     */
    public int getSL() {
        return SL;
    }

    /**
     * @param SL the SL to set
     */
    public void setSL(int SL) {
        this.SL = SL;
    }

    /**
     * @return the DG
     */
    public int getDG() {
        return DG;
    }
    /**
     * @param DG the DG to set
     */
    public void setDG(int DG) {
        this.DG = DG;
    }
    public ChiTietHDN() throws ParseException {
        MaG = "";
        SL = 0;
        DG = 0;
        s = nv.readds();
    }
    public ChiTietHDN(String MaG,int SL,int DG) throws ParseException {
        this.MaG = MaG;
        this.SL = SL;
        this.DG = DG;
        s = nv.readds();
    }

    public ChiTietHDN(String nh) throws ParseException {
        String[] d = nh.split("_");
        MaG = d[0];
        SL = Integer.parseInt(d[1]);
        DG = Integer.parseInt(d[2]);
        s = nv.readds();
    }
    @Override
    public String toString() {
        return MaG + "_" + SL + "_" + DG;
    }
    public QLSneakers nv = new QLSneakers();
    public ArrayList<Giay> s;
    // Lấy ra tên của giày có mã : ma
    public String Ten(String ma) {
        String t = null;
        for (int i = 0; i < s.size(); i++) {
            if (s.get(i).MaG.equals(ma)) {
                t = s.get(i).getTenG();
            }
        }
        return t;
    }
    // Kiểm tra mã trùng
    public boolean Trung(String ma) {
        for (int i = 0; i < s.size(); i++) {
            if (s.get(i).MaG.toLowerCase().equals(ma.toLowerCase())) {
                return false;
            }
        }
        return true;
    }
    public void Nhap() {
        Scanner scr = new Scanner(System.in);

        do {
            System.out.print(" Nhap ma giay: ");
            MaG = scr.nextLine();
            if (MaG.trim().equals("") || Trung(MaG)) {
                System.out.println("Ma giay trong hoac Khong ton tai giay có ma nhu vay. Hay nhap lai !");
            }
        } while (MaG.trim().equals("") || Trung(MaG));
        do {
            System.out.print(" So luong: ");
            SL = scr.nextInt();
            if (SL <= 0) {
                System.out.println(" So luong phai lon hon 0: ");
            }
        } while (SL <= 0);
        do {
            System.out.print("Nhap don gia: ");
            DG=scr.nextInt();
            if (DG <= 0) {
                System.out.println(" Don gia phai lon hon 0: ");
            }
        } while (DG <= 0);
    }
    public int ThanhTien() {
        return (int)SL*DG;
    }
    public void HienThi() {
        System.out.printf("%-20s\t%-20s\t%-15s\t%-20s\t%-20s\n",MaG,Ten(MaG),SL,DG,ThanhTien());
    }
}
