/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package newpro;


import java.io.*;
import java.text.*;
import java.util.*;

/**
 *
 * @author My Computer
 */

public class QLnhanvien {
ArrayList<Nhanvien> DSach ;//= new ArrayList<>();
            public QLnhanvien(){
                try{DSach=readds();
//                if (DSach==null)
//                    DSach= new ArrayList<>();
                }catch(Exception ex)
                {
                    System.out.println(ex.getMessage());
                }
            }
    public Scanner sc = new Scanner(System.in);

    public ArrayList<Nhanvien> readds() throws ParseException {
        BufferedReader br = null;
        DSach = new ArrayList<>();
       try {
            File file=new File("D:\\DoAn\\nhanvien.txt");
            if(!file.exists()){file.createNewFile();}
            br = new BufferedReader(new FileReader("D:\\DoAn\\nhanvien.txt"));
            String ttdsb = br.readLine();
            while (ttdsb != null) {
                Nhanvien ds = new Nhanvien(ttdsb);
                DSach.add(ds);
                ttdsb = br.readLine();
                br.close();//Ghitep(DSach);
            }
        } catch (IOException e) {System.out.println(Arrays.toString(e.getStackTrace()));
}
//        } finally {
//            try {
//                br.close();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
        return DSach;
    }
    

    public void Ghitep() throws IOException {
        BufferedWriter bw = null;
        try {
            bw = new BufferedWriter(new FileWriter("D:\\DoAn\\nhanvien.txt"));
            for (int i = 0; i < DSach.size(); i++) {
                bw.write(DSach.get(i).toString());
                bw.newLine();
               // bw.close();
            }
        } catch (IOException e) {
        } finally {
            try {
                bw.close();
                System.out.println("Ghi tep thanh cong");
            } catch (IOException e) {
            }
        }
    }

    public void nhapds() throws ParseException {
        do {
            Nhanvien ds = new Nhanvien();
            ds.Nhap(DSach);
            DSach.add(ds);
            System.out.println("Nhan phim bat ky de tiep tuc hoac enter de ket thuc ! ");
            String kt = sc.nextLine();
            if (kt.equals("")) {
                break;
            }
        } while (true);
    }

    public ArrayList<Nhanvien> TKTennv( String ten) throws ParseException {
        ArrayList<Nhanvien> ds = new ArrayList<>();
        for (int i = 0; i < DSach.size(); i++) {
            if (DSach.get(i).getTennv().toLowerCase().contains(ten.toLowerCase())) {
                ds.add(DSach.get(i));
            }
        }
        return ds;
    }

    public Nhanvien TKManv( String Ma) {
        for (int i = 0; i < DSach.size(); i++) {
            if (DSach.get(i).getManv().contains(Ma)) {
                return DSach.get(i);
            }
        }
        return null;
    }
//Tim kiem theo dia chi nhan vien:
    public ArrayList<Nhanvien> TKDCnv( String dc) throws ParseException {
        ArrayList<Nhanvien> ds = new ArrayList<>();
        for (int i = 0; i < DSach.size(); i++) {
            if (DSach.get(i).getDC().toLowerCase().contains(dc.toLowerCase())) {
                ds.add(DSach.get(i));
            }
        }
        return ds;
    }
    public void Capnhat() throws ParseException {
        int cs = -1;
        System.out.println("Nhap ma nhan vien can sua : ");
        String manv = sc.nextLine();
        for (int i = 0; i < DSach.size(); i++) {
            if (DSach.get(i).getManv().contains(manv)) { //cái contains là phương thức trả về đúng hoặc sai khi mà cái xâu mình cần ktra nó chứa xâu trong phần () ý
                cs = i;
            }
        }
        if (cs != -1) {
            DSach.get(cs).update();
        } else {
            System.out.println("Khong ton tai ma nhan vien nay ! ");
        }
    }
    public void Xoa() {

        int cs = -1;
        System.out.println("Nhap ma nhan vien can xoa : ");
        String manv = sc.nextLine();
        for (int i = 0; i < DSach.size(); i++) {
            if (DSach.get(i).getManv().contains(manv)) {
                cs = i;
            }
        }
        if (cs != -1) {
            DSach.remove(cs);
            System.out.println("Xoa thanh cong!");
        } else {
            System.out.println("Khong ton tai ma nhan vien nay ! ");
        }
    }

    public void hienthids(ArrayList<Nhanvien> dsnv) {
        System.out.println("Danh sach thong tin nhan vien :");
        System.out.printf("\n%-10s\t%-20s\t%-10s\t%-30s\t%-9s\n", "Ma nhan vien", "Ten nhan vien", "Dien thoai", "Dia chi", "CMND");
        for (int i = 0; i < dsnv.size(); i++) {
            dsnv.get(i).Hienthi();
        }
    }

    public void menusearch() throws ParseException, IOException {
        Scanner scr = new Scanner(System.in);
        QLnhanvien chose = new QLnhanvien();
        String chon;
        do {
            System.out.println("\n");
            System.out.println("------------------------------");
            System.out.println(" Chon phuong thuc tim kiem! ");
            System.out.println(" 1.Tim kiem theo ma nhan vien ");
            System.out.println(" 2.Tim kiem theo ten nhan vien");
            System.out.println(" 3.Tim kiem theo dia chi nhan vien");
            System.out.println(" 0.Exit ");
            chon = scr.nextLine();
            switch (chon) {
                case "1":
                    System.out.println("Nhap ma nhan vien can tim ");
                    String Ma = scr.nextLine();
                    Nhanvien dsm = chose.TKManv( Ma);
                    if (dsm == null) {
                        System.out.println("Nhan vien nay khong ton tai! ");
                    } else {
                        dsm.Hienthi();
                    }
                    break;
                case "2":
                    System.out.println("Nhap ten nhan vien can tim ");
                    String ten = scr.nextLine();
                    ArrayList<Nhanvien> dsnv = chose.TKTennv( ten);
                    if (dsnv.isEmpty()) {
                        System.out.println("Nhan vien nay khong ton tai! ");
                    } else {
                        chose.hienthids(dsnv);
                    }
                    break;
                    case "3":
                    System.out.println("Nhap dia chi nhan vien can tim ");
                    String dchi = scr.nextLine();
                    ArrayList<Nhanvien> ds = chose.TKDCnv( dchi);
                    if (ds.isEmpty()) {
                        System.out.println("Nhan vien nay khong ton tai! ");
                    } else {
                        chose.hienthids(ds);
                    }
                    break;

                case "0":
                    menu n=new menu();n.menuchinh();
                    //chose.menudk1();
                    break;
            }
        } while (true);
    }

    public void menudk1() throws ParseException, IOException {
        Scanner scr = new Scanner(System.in);
        QLnhanvien chose = new QLnhanvien();
        String chon;
        boolean kt;
        do {
            chon = null;
            kt = true;
            System.out.println("\n");
            System.out.println("QUAN LY DANH SACH NHAN VIEN");
            System.out.println("------------------------------");
            System.out.println(" 1.Nhap danh sach nhan vien ");
            System.out.println(" 2.Hien thi danh sach nhan vien ");
            System.out.println(" 3.Cap nhat danh sach nhan vien ");
            System.out.println(" 4.Xoa danh sach nhan vien ");
            System.out.println(" 5.Tim kiem ");
            System.out.println(" 0.Exit ");
            System.out.println("------------------------------");
            do {
                try {
                    System.out.println(" Chon cong viec can thuc hien ! ");
                    chon = scr.nextLine();
                    if (chon.compareTo("0") == -1 || chon.compareTo("5") == 1) {
                        throw new Exception("");
                    }
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            } while (chon.compareTo("0") == -1 || chon.compareTo("5") == 1);

            switch (chon) {
                case "1":
                    nhapds();
                    Ghitep();
                    break;
                case "2":
                    hienthids(DSach);
                    break;
                case "3":
                    Capnhat();
                    Ghitep();
                    break;
                case "4":
                    Xoa();
                    break;
                case "5":
                    menusearch();
                    break;
                case "0":
                    kt = false;
                    break;
            }
        } while (kt);
    }
}


