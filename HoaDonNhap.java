/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package newpro;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author My Computer
 */
public class HoaDonNhap {
    public String ma_HDN, ma_NV, ma_NCC;
    public Date ngayNhap;
    public SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy");
    public ArrayList<ChiTietHDN> ct;

    /**
     * @return the ma_HDN
     */
    public String getMa_HDN() {
        return ma_HDN;
    }

    /**
     * @param ma_HDN the ma_HDN to set
     */
    public void setMa_HDN(String ma_HDN) {
        this.ma_HDN = ma_HDN;
    }

    /**
     * @return the ma_NV
     */
    public String getMa_NV() {
        return ma_NV;
    }

    /**
     * @param ma_NV the ma_NV to set
     */
    public void setMa_NV(String ma_NV) {
        this.ma_NV = ma_NV;
    }

    /**
     * @return the ma_NCC
     */
    public String getMa_NCC() {
        return ma_NCC;
    }

    /**
     * @param ma_NCC the ma_NCC to set
     */
    public void setMa_NCC(String ma_NCC) {
        this.ma_NCC = ma_NCC;
    }

    /**
     * @return the ngayNhap
     */
    public Date getNgayNhap() {
        return ngayNhap;
    }

    /**
     * @param ngayNhap the ngayNhap to set
     */
    public void setNgayNhap(Date ngayNhap) {
        this.ngayNhap = ngayNhap;
    }

    /**
     * @return the ft
     */
    public SimpleDateFormat getFt() {
        return ft;
    }

    /**
     * @param ft the ft to set
     */
    public void setFt(SimpleDateFormat ft) {
        this.ft = ft;
    }
    public HoaDonNhap() throws ParseException {
        ma_HDN = "";
        ngayNhap=null;
        ma_NV = "";
        ma_NCC = "";
        ct = new ArrayList<>();
        s=ncc.readds();
        nv=dsnv.readds();
    }

    public HoaDonNhap(String mhd, Date d, String mnv, String mncc, ArrayList<ChiTietHDN> hd) throws ParseException {
        ma_HDN = mhd;
        ngayNhap = d;
        ma_NV = mnv;
        ma_NCC = mncc;
        ct = hd;
        s=ncc.readds();
        nv=dsnv.readds();
    }
    //Tạo xâu dạng: MaG,SL,DG@MaG,SL,DG
    public String toStringCTHDN() {
        String t = ct.get(0).toString();
        for (int i = 1; i < ct.size(); i++) { // Khởi tạo giá trị t= phần từ 0 và int =1 để tránh ra xâu bị thừa 1 ký tự @
            t = t + "@" + ct.get(i).toString();
        }
        return t;
    }
    public ArrayList<ChiTietHDN> GanCTHDN(String a) throws ParseException {
        ArrayList< ChiTietHDN> ct = new ArrayList<>();
        String[] b = a.split("@");
        for (int i = 0; i < b.length; i++) {
            ChiTietHDN cthd = new ChiTietHDN(b[i]);
            ct.add(cthd);
        }
        return ct;
    }
    public QLNCC ncc = new QLNCC();
    public ArrayList<NhaCungCap> s = ncc.readds();
    // Lấy ra tên của nhà cung cấp có mã : ma
    public String TenNCC(String ma) {
        String t = null;
        for (int i = 0; i < s.size(); i++) {
            if (s.get(i).Mancc.equals(ma)) {
                t = s.get(i).getTenncc();
            }
        }
        return t;
    }
    public boolean TrungNCC(String ma) {
        for (int i = 0; i < s.size(); i++) {
            if (s.get(i).Mancc.toLowerCase().equals(ma.toLowerCase())) {
                return false;
            }
        }
        return true;
    }
    public QLnhanvien dsnv = new QLnhanvien();
    public ArrayList<Nhanvien> nv = dsnv.readds();
    // Lấy ra tên của nhà cung cấp có mã : ma
    public String TenNV(String ma) {
        String t = null;
        for (int i = 0; i < nv.size(); i++) {
            if (nv.get(i).Manv.equals(ma)) {
                t = nv.get(i).getTennv();
                return t;
            }
        }
        return null;
    }

    public boolean TrungNV(String ma) {
        for (int i = 0; i < nv.size(); i++) {
            if (nv.get(i).Manv.toLowerCase().equals(ma.toLowerCase())) {
                return false;
            }
        }
        return true;
    }
    @Override
    public String toString() {
        return ma_HDN + "#" + ft.format(ngayNhap) + "#" + ma_NV + "#" + ma_NCC + "#" + toStringCTHDN();
    }
    // Gán xâu từ tệp 
    public HoaDonNhap(String shd) throws ParseException{// throws Exception {
        String[] a = shd.split("#");
        ma_HDN = a[0];
        try {
            ngayNhap = ft.parse(a[1]);
        } catch (ParseException ex) {
            Logger.getLogger(HoaDonNhap.class.getName()).log(Level.SEVERE, null, ex);
        }
        ma_NV = a[2];
        ma_NCC = a[3];
        ct = GanCTHDN(a[4]);
    }

    public boolean Trung(String ma) throws ParseException  {
        QLHDN dshd = new QLHDN();
        ArrayList<HoaDonNhap> hd = dshd.readds();
        for (int i = 0; i < hd.size(); i++) {
            if (hd.get(i).ma_HDN.equals(ma)) {
                return true;
            }
        }
        return false;
    }
    public void Nhap() throws ParseException {
        Scanner scr = new Scanner(System.in);
        do {
            System.out.print("Nhap ma hoa don nhap: ");
            ma_HDN = scr.nextLine();
            if (ma_HDN.equals("") || ma_HDN.length() != 5 || Trung(ma_HDN) || !(ma_HDN.matches("[h]{1}[d]{1}[n]{1}[0-9]{2}"))) {
                System.out.println("Ban chua nhap hoac ma đa ton tai. Yeu cau nhap lai !");
            }
        } while (ma_HDN.equals("") || ma_HDN.length() != 5 || Trung(ma_HDN) || !(ma_HDN.matches("[h]{1}[d]{1}[n]{1}[0-9]{2}")));
        ngayNhap = new Date();
        do {
            System.out.print(" Ma nhan vien: ");
            ma_NV = scr.nextLine();
            if (ma_NV.trim().equals("") || TrungNV(ma_NV)) {
                System.out.println("Ma nhan vien trong hoac Khong ton tai nhan vien co ma Nhu vay. Hay nhap lai !");
            }
        } while (ma_NV.trim().equals("") || TrungNV(ma_NV));
        do {
            System.out.print(" Ma nha cung cap: ");
            ma_NCC = scr.nextLine();
            if (ma_NCC.trim().equals("") || TrungNCC(ma_NCC)) {
                System.out.println("Ma nha cung cap trong hoac Khong ton tai nha cung cap co ma Nhu vay. Hay nhap lai !");
            }
        } while (ma_NCC.trim().equals("") || TrungNCC(ma_NCC));
        String tiep;
        do {
            ChiTietHDN cthd = new ChiTietHDN();
            cthd.Nhap();  // Nhập thông tin cho một Item
            ct.add(cthd);
            System.out.print("Nhan bat ki đe tiep tuc hoac Enter đe ket thuc");
            tiep = scr.nextLine();
        } while (tiep.equals("") == false);
    }
     public int tongtien() {
        int t = 0;
        for (int i = 0; i < ct.size(); i++) {
            t = t + ct.get(i).ThanhTien();
        }
        return t;
    }
    public void update() throws ParseException
    {
        Scanner scr = new Scanner(System.in);
        do {
            System.out.print(" Ma nhan vien: ");
            ma_NV = scr.nextLine();
            if (ma_NV.trim().equals("") || TrungNV(ma_NV)) {
                System.out.println("Ma nhan vien trong hoac Khong ton tai nhan vien co ma Nhu vay. Hay nhap lai !");
            }
        } while (ma_NV.trim().equals("") || TrungNV(ma_NV));
        do {
            System.out.print(" Ma nha cung cap: ");
            ma_NCC = scr.nextLine();
            if (ma_NCC.trim().equals("") || TrungNCC(ma_NCC)) {
                System.out.println("Ma nha cung cap trong hoac Khong ton tai nha cung cap co ma Nhu vay. Hay nhap lai !");
            }
        } while (ma_NCC.trim().equals("") || TrungNCC(ma_NCC));
        String tiep;
        do {
            ChiTietHDN cthd = new ChiTietHDN();
            cthd.Nhap();  // Nhập thông tin cho một Item
            ct.add(cthd);
            System.out.print("Nhan bat ki đe tiep tuc hoac Enter đe ket thuc");
            tiep = scr.nextLine();
        } while (tiep.equals("") == false);
    }
    public void Hien()
    {
        System.out.println(" --------------------------HÓA ĐƠN NHẬP HÀNG ------------------------");
        System.out.println("Mã hóa đơn " + ma_HDN);
        System.out.println("Thời gian: " + ft.format(ngayNhap));
        System.out.println("Tên nhân viên nhập hàng: " + TenNV(ma_NV));
        System.out.println("Tên nhà cung cấp: " + TenNCC(ma_NCC));
        System.out.printf("%-3s\t%-20s\t%-20s\t%-15s\t%-20s\t%-20s\n","STT","Mã sản phẩm","Tên sản phẩm","Số lượng","Đơn giá","Thành tiền");
        for (int i = 0; i < ct.size(); i++) {
            System.out.print((i + 1) + "\t");
            ct.get(i).HienThi();
        }
    }
}