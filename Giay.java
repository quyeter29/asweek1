/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package newpro;
//Mã giày, Tên giày, Loại giày, Kích cỡ, Giá thành, Số lượng, Ghi chú

/**
 *
 * @author My Computer
 */
import java.util.*;
import java.text.*;

public class Giay {

    public String MaG, TenG, Loaigiay;
    public int SL, Kichco;
    public int Gia;

    public Giay() {
    }

    public Giay(String MaG, String TenG, String Loaigiay, int Kichco,int SL, int Gia) {
        this.MaG = MaG;
        this.TenG = TenG;
        this.Loaigiay = Loaigiay;
        this.Kichco = Kichco;
        this.SL=SL;
        this.Gia = Gia;
//        this.Ghichu = Ghichu;
    }

    public Giay(String ttdsb) throws ParseException {
        String[] tmp = ttdsb.split("#");
        MaG = tmp[0];
        TenG = tmp[1];
        Loaigiay = tmp[2];
        Kichco = Integer.decode(tmp[3]);
        SL=Integer.decode(tmp[4]);
        Gia = Integer.decode(tmp[5]);
//        Ghichu = tmp[6];
    }

    public int getSL() {
        return SL;
    }

    public void setSL(int SL) {
        this.SL = SL;
    }

    public String getMaG() {
        return MaG;
    }

    public void setMaG(String MaG) {
        this.MaG = MaG;
    }

    public String getTenG() {
        return TenG;
 
    }

    public void setTenG(String TenG) {
        this.TenG = TenG;
    }

    public String getLoaigiay() {
        return Loaigiay;
    }

    public void setLoaigiay(String Loaigiay) {
        this.Loaigiay = Loaigiay;
    }

//    public String getGhichu() {
//        return Ghichu;
//    }
//
//    public void setGhichu(String Ghichu) {
//        this.Ghichu = Ghichu;
//    }

    public int getKichco() {
        return Kichco;
    }

    public void setKichco(int Kichco) {
        this.Kichco = Kichco;
    }
    public int getGia() {
        return Gia;
    }
    public void setGia(int Gia) {
        this.Gia = Gia;
    }
    @Override
    public String toString() {
        return MaG + "#" + TenG + "#" + Loaigiay + "#" + Kichco + "#" + SL + "#" + Gia ;
    }
    public boolean KT( ArrayList<Giay> nv,String a) throws ParseException
    {
       //QLSneakers NV = new QLSneakers();
     //  ArrayList<Giay> nv = NV.readds();
       if(nv==null) return false;
       for(int i=0;i<nv.size();i++)
       {if(nv.get(i).MaG.toLowerCase().equals(a.toLowerCase()))
       {return true;}}
       return false;
    }
//Nhap thong tin giay
    public void Nhap( ArrayList<Giay> nv) throws ParseException {
        Scanner scr = new Scanner(System.in);
//        QLSneakers NV = new QLSneakers();
//        ArrayList<Giay> nv = NV.readds();
        boolean kt;
        do {
//            kt = true;
//            int d = 0;
            do{
            System.out.println("Nhap ma giay(Toi da 8 ki tu):  ");
            MaG = scr.nextLine().trim();//trim(): xoa bo cac khoang trang truoc do
            }while(MaG.length()>8);
            //for (Giay ds : nv) {
            kt=MaG.trim().equals("")==true||KT(nv,MaG);   
            if (kt) {
                    System.out.println(" Ma giay da ton tai moi nhap lai!");
//                    kt=true;
                    //break;
                } 
//                    else {
//                    d++;
//                }
//            }
//            if (d == nv.size()) {
//                kt = false;
//            }
        } while (kt);
      
        do {
            System.out.println("Nhap ten giay :  ");
            TenG = scr.nextLine();
        } while ("".equals(TenG));
        do {
            System.out.println("Nhap vao loai giay :  ");
            Loaigiay = scr.nextLine();
        } while ("".equals(Loaigiay));
        do {
            System.out.println("Nhap vao kich co cua giay:  ");
            Kichco = scr.nextInt();
        } while (Kichco < 0 || Kichco > 46);
        do{
            System.out.println("Nhap vao so luong giay: ");
            SL=scr.nextInt();
        }while(SL<0);
        do {
            System.out.println("Nhap vao gia cua giay:  ");
            Gia = scr.nextInt();
        } while (Gia < 0);
//        do {
//            System.out.println("Nhap vao ghi chu can thiet :  ");
//            Ghichu = scr.nextLine();
//        } while ("".equals(Ghichu));

    }
//Hien thi
    public void Hienthi() {
        System.out.printf("\n%-10s\t%-20s\t%-15s\t%-8d\t%-3d\t%-10s\n", MaG, TenG, Loaigiay, Kichco,SL, Gia);
    }

    public void update() {
        Scanner scr = new Scanner(System.in);

        do {
            System.out.println("Nhap ten giay :  ");
            TenG = scr.nextLine();
        } while ("".equals(TenG));
        do {
            System.out.println("Nhap vao loai giay :  ");
            Loaigiay = scr.nextLine();
        } while ("".equals(Loaigiay));
        do {
            System.out.println("Nhap vao kich co cua giay:  ");
            Kichco = scr.nextInt();
        } while (Kichco < 0 || Kichco > 46);
         do{
            System.out.println("Nhap vao so luong giay: ");
            SL=scr.nextInt();
        }while(SL<0);
        do {
            System.out.println("Nhap vao gia cua giay:  ");
            Gia = scr.nextInt();
        } while (Gia < 0);
//        do {
//            System.out.println("Nhap vao ghi chu can thiet :  ");
//            Ghichu = scr.nextLine();
//        } while ("".equals(Ghichu));
    }
}

