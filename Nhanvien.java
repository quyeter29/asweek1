/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package newpro;

import java.util.*;
import java.text.*;

public class Nhanvien {

    public String Manv, Tennv, SDT, DC; //SDT: so dien thoai,DC: dia chi
    public String CMND; // CMND: so cmnd

    public Nhanvien() {
        Manv = "";
        Tennv = "";
        SDT = "";
        DC = "";
        CMND = "";
    }

    public Nhanvien(String Manv, String Tennv, String SDT, String DC, String CMND) {
        this.Manv = Manv;
        this.Tennv = Tennv;
        this.SDT = SDT;
        this.DC = DC;
        this.CMND = CMND;
    }

    public Nhanvien(String ttdsb) throws ParseException {
        String[] tmp = ttdsb.split("#");
        Manv = tmp[0];
        Tennv = tmp[1];
        SDT = tmp[2];
        DC = tmp[3];
        CMND = tmp[4];

    }

    public String getManv() {
        return Manv;
    }

    public void setManv(String Manv) {
        this.Manv = Manv;
    }

    public String getTennv() {
        return Tennv;
    }

    public void setTennv(String Tennv) {
        this.Tennv = Tennv;
    }

    public String getSDT() {
        return SDT;
    }

    public void setSDT(String SDT) {
        this.SDT = SDT;
    }

    public String getDC() {
        return DC;
    }

    public void setDC(String DC) {
        this.DC = DC;
    }

    public String getCMND() {
        return CMND;
    }

    public void setCMND(String CMND) {
        this.CMND = CMND;
    }

    @Override
    public String toString() {
        return Manv + "#" + Tennv + "#" + SDT + "#" + DC + "#" + CMND;
    }
public boolean KT( ArrayList<Nhanvien> nv,String a) throws ParseException
    {
       //QLSneakers NV = new QLSneakers();
     //  ArrayList<Giay> nv = NV.readds();
       if(nv==null) return false;
       for(int i=0;i<nv.size();i++)
       {if(nv.get(i).Manv.toLowerCase().equals(a.toLowerCase()))
       {return true;}}
       return false;
    }
    public void Nhap(ArrayList<Nhanvien> nv) throws ParseException {
        Scanner nhap = new Scanner(System.in);
        //QLnhanvien NV = new QLnhanvien();
        //ArrayList<Nhanvien> nv = NV.readds();
        boolean kt;
        do {
//            kt = true;
//            int d = 0;
            System.out.println("Nhap ma nhan vien : ");
            Manv = nhap.nextLine().trim();
            //for(int i=0;i<nv.size();i++){
            kt=Manv.trim().equals("")==true||KT(nv,Manv);   
            if (kt)
                    System.out.println(" Ma nhan vien da ton tai moi nhap lai!");
                    
        } while (kt);
        do {
            System.out.println("Nhap ten nhan vien :  ");
            Tennv = nhap.nextLine();
        } while ("".equals(Tennv));
         do {
            System.out.print("Nhập Số điện thoại : ");
            SDT = nhap.nextLine();
            if (SDT.matches("[0-9]{10}") == false) {
                System.out.println(" Số điện thoại phải có 10 chữ số và gồm các chữ số 0-9. Hãy nhập lại ");
            }
        } while (SDT.matches("[0-9]{10}") == false);
        do {
            System.out.println("Nhap so chung minh nhan dan:  ");
            CMND = nhap.nextLine();
        } while ("".equals(CMND));
        do {
            System.out.println("Nhap dia chi cua nhan vien :  ");
            DC = nhap.nextLine();
        } while ("".equals(DC));
    }

    public void update() {
        Scanner nhap = new Scanner(System.in);
        do {
            System.out.println("Nhap ten nhan vien :  ");
            Tennv = nhap.nextLine();
        } while ("".equals(Tennv));
         do {
            System.out.print("Nhập Số điện thoại : ");
            SDT = nhap.nextLine();
            if (SDT.matches("[0-9]{10}") == false) {
                System.out.println(" Số điện thoại phải có 10 chữ số và gồm các chữ số 0-9. Hãy nhập lại ");
            }
        } while (SDT.matches("[0-9]{10}") == false);

        do {
            System.out.println("Nhap dia chi cua nhan vien :  ");
            DC = nhap.nextLine();
        } while ("".equals(DC));
        do {
            System.out.println("Nhap so chung minh nhan dan:  ");
            CMND = nhap.nextLine();
        } while ("".equals(DC));
    }
    public void Hienthi() {
        System.out.printf("%-10s\t%-20s\t%-10s\t%-30s\t%-9s\n", Manv, Tennv, SDT, DC, CMND);

    }

}
