/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package newpro;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ADMIN
 */
public class HoaDonXuat {

    /**
     * @return the ma_HDX
     */
    public String getMa_HDX() {
        return ma_HDX;
    }

    /**
     * @param ma_HDX the ma_HDX to set
     */
    public void setMa_HDX(String ma_HDX) {
        this.ma_HDX = ma_HDX;
    }

    /**
     * @return the ct
     */
    public ArrayList<ChiTietHDX> getCt() {
        return ct;
    }

    /**
     * @param ct the ct to set
     */
    public void setCt(ArrayList<ChiTietHDX> ct) {
        this.ct = ct;
    }
    /**
     * @return the ma_NV
     */
    public String getMa_NV() {
        return ma_NV;
    }

    /**
     * @param ma_NV the ma_NV to set
     */
    public void setMa_NV(String ma_NV) {
        this.ma_NV = ma_NV;
    }

    /**
     * @return the ngayXuat
     */
    public Date getNgayXuat() {
        return ngayXuat;
    }

    /**
     * @param ngayXuat the ngayXuat to set
     */
    public void setNgayXuat(Date ngayXuat) {
        this.ngayXuat = ngayXuat;
    }
    public String ma_HDX, ma_NV;
    public Date ngayXuat;
    public SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy");
    public ArrayList<ChiTietHDX> ct;
//    public HoaDonXuat() {
//        ma_HDX = "";
//        ngayXuat=null;
//        ma_NV = "";
//        ct = new ArrayList<>();
//    }

    public HoaDonXuat(String mhd, Date d, String mnv, ArrayList<ChiTietHDX> hd) throws ParseException {
//        try{
        nv = dsnv.readds();
//        }catch(Exception ex)
//         {
//         System.out.println(ex.getMessage());
//         }
        ma_HDX = mhd;
        ngayXuat = d;
        ma_NV = mnv;
        ct = hd;
    }
     public HoaDonXuat() throws ParseException {
//         try{
        nv = dsnv.readds();
//         }catch(Exception ex)
//         {
//         System.out.println(ex.getMessage());
//         }
        ma_HDX = "";
        ngayXuat =  new Date() ;
                ma_NV = "";
        ct = null;
    }

    //Tạo xâu dạng: MaSP,Slg@MaSP,Slg...
    public String toStringCTHDX() {
        String t = ct.get(0).toString();
        for (int i = 1; i < ct.size(); i++) { // Khởi tạo giá trị t= phần từ 0 và int =1 để tránh ra xâu bị thừa 1 ký tự @
            t = t + "@" + ct.get(i).toString();
        }
        return t;
    }

    public ArrayList<ChiTietHDX> GanCTHDX(String a) throws ParseException {
        ArrayList<ChiTietHDX> ct = new ArrayList<>();
        String[] b = a.split("@");
        for (int i = 0; i < b.length; i++) {
            ChiTietHDX cthd = new ChiTietHDX(b[i]);
            ct.add(cthd);
        }
        return ct;
    }
    public QLnhanvien dsnv = new QLnhanvien();
    public ArrayList<Nhanvien> nv= dsnv.readds();
    // Lấy ra tên của nhà cung cấp có mã : ma
    public String TenNV(String ma) {
        String t = null;
        for (int i = 0; i < nv.size(); i++) {
            if (nv.get(i).Manv.equals(ma)) {
                t = nv.get(i).getTennv();
            return t;
            }
        }
        return null;
    }

    public boolean TrungNV(String ma) {
        for (int i = 0; i < nv.size(); i++) {
            if (nv.get(i).Manv.toLowerCase().equals(ma.toLowerCase())) {
                return false;
            }
        }
        return true;
    }
    @Override
    public String toString() {
        return ma_HDX + "#" + ft.format(ngayXuat) + "#" +ma_NV + "#" + toStringCTHDX();   //cthd.toString() của chi tiết hóa đơn
    }
    // Gán xâu từ tệp 
    public HoaDonXuat(String shd) throws ParseException{// throws Exception {
        String[] a = shd.split("#");
        ma_HDX = a[0];
        try {
            ngayXuat = ft.parse(a[1]);
        } catch (ParseException ex) {
            Logger.getLogger(HoaDonXuat.class.getName()).log(Level.SEVERE, null, ex);
        }
        ma_NV = a[2];
        ct = GanCTHDX(a[3]);
    }
    public boolean Trung(String ma) throws ParseException {
        QLHDX ttdsb = new QLHDX();
        ArrayList<HoaDonXuat> hd = ttdsb.readds();
        for (int i = 0; i < hd.size(); i++) {
            if (hd.get(i).ma_HDX.equals(ma)) {
                return true;
            }
        }
        return false;
    }
    public void Nhap() throws ParseException {
        Scanner scr = new Scanner(System.in);
        do {
            System.out.print(" Nhập Mã hóa đơn xuất: ");
            ma_HDX = scr.nextLine();
            if (ma_HDX.equals("") || ma_HDX.length() != 5 || Trung(ma_HDX) || !(ma_HDX.matches("[h]{1}[d]{1}[x]{1}[0-9]{2}"))) {
                System.out.println("Mã hóa đơn không đúng định dạng [hdX**]. Yêu cầu nhập lại !");
            }
        } while (ma_HDX.equals("") || ma_HDX.length() != 5 || Trung(ma_HDX) || !(ma_HDX.matches("[h]{1}[d]{1}[x]{1}[0-9]{2}")));
        ngayXuat = new Date();
        do {
            System.out.print(" Mã nhân viên: ");
            ma_NV = scr.nextLine();
            if (ma_NV.trim().equals("") || TrungNV(ma_NV)) {
                System.out.println("Mã nhân viên trống hoặc Không tồn tại nhân viên có mã Như vậy. Hãy nhập lại !");
            }
        } while (ma_NV.trim().equals("") || TrungNV(ma_NV));
                String tiep;
                ct=new ArrayList<>();
        do {
            ChiTietHDX cthdx = new ChiTietHDX();
            cthdx.Nhap();  // Nhập thông tin cho một Item
            ct.add(cthdx);
            System.out.print("Nhấn bất kì để nhập tiếp, Enter để kết thúc");
            tiep = scr.nextLine();
        } while (tiep.equals("") == false);
    }
    public void update() throws ParseException
    {
        Scanner scr = new Scanner(System.in);
        do {
            System.out.print(" Mã nhân viên: ");
            ma_NV = scr.nextLine();
            if (ma_NV.trim().equals("") || TrungNV(ma_NV)) {
                System.out.println("Mã nhân viên trống hoặc Không tồn tại nhân viên có mã Như vậy. Hãy nhập lại !");
            }
        } while (ma_NV.trim().equals("") || TrungNV(ma_NV));
                String tiep;
        do {
            ChiTietHDX cthdx = new ChiTietHDX();
            cthdx.Nhap();  // Nhập thông tin cho một Item
            ct.add(cthdx);
            System.out.print("Nhấn bất kì để nhập tiếp, Enter để kết thúc");
            tiep = scr.nextLine();
        } while (tiep.equals("") == false);
    }
    public void Hien() {
        System.out.println(" --------------------------HÓA ĐƠN XUẤT HÀNG ------------------------");
        System.out.println("Mã hóa đơn " + ma_HDX);
        System.out.println("Thời gian: " + ft.format(ngayXuat));
        System.out.println("Tên nhân viên xuất hàng: " + TenNV(ma_NV));
        System.out.printf("%-3s\t%-20s\t%-20s\t%-15s\t%-20s\n","STT","Mã sản phẩm","Tên sản phẩm","Số lượng","Thành tiền");
        for (int i = 0; i < ct.size(); i++) {
            System.out.print((i + 1) + "\t");
            ct.get(i).HienThi();
        }
    }
    public int tongtien() {
        int t = 0;
        for (int i = 0; i < ct.size(); i++) {
            t = t + ct.get(i).ThanhTien();
        }
        return t;
    }
//    public void Sua() throws ParseException {
//        Scanner scr = new Scanner(System.in);
//        do {
//            System.out.print(" Mã nhân viên: ");
//            ma_NV = scr.nextLine();
//            if (ma_NV.trim().equals("") || TrungNV(ma_NV)) {
//                System.out.println("Mã nhân viên trống hoặc Không tồn tại nhân viên có mã Như vậy. Hãy nhập lại !");
//            }
//        } while (ma_NV.trim().equals("") || TrungNV(ma_NV));
//        String tiep;
//        do {
//            ChiTietHDX cthd = new ChiTietHDX();
//            cthd.Nhap();  // Nhập thông tin cho một Item
//            ct.add(cthd);
//            System.out.print("Nhấn bất kì để nhập tiếp, Enter để kết thúc");
//            tiep = scr.nextLine();
//        } while (tiep.equals("") == false);
//    }
}
