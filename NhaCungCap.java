/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package newpro;
import java.util.*;
import java.text.*;
/**
 *
 * @author My Computer
 */
public class NhaCungCap {
    public String Mancc, Tenncc, SDT, DC; //SDT: so dien thoai,DC: dia chi
    public String CMND; // CMND: so cmnd
    public NhaCungCap() {
        Mancc = "";
        Tenncc = "";
        SDT = "";
        DC = "";
        CMND = "";
    }

    public NhaCungCap(String Mancc, String Tenncc, String SDT, String DC, String CMND) {
        this.Mancc = Mancc;
        this.Tenncc = Tenncc;
        this.SDT = SDT;
        this.DC = DC;
        this.CMND = CMND;
    }

    public NhaCungCap(String ncc) throws ParseException {
        String[] tmp = ncc.split("#");
        Mancc = tmp[0];
        Tenncc = tmp[1];
        SDT = tmp[2];
        DC = tmp[3];
        CMND = tmp[4];
    }

    /**
     * @return the Mancc
     */
    public String getMancc() {
        return Mancc;
    }

    /**
     * @param Mancc the Mancc to set
     */
    public void setMancc(String Mancc) {
        this.Mancc = Mancc;
    }

    /**
     * @return the Tenncc
     */
    public String getTenncc() {
        return Tenncc;
    }

    /**
     * @param Tenncc the Tenncc to set
     */
    public void setTenncc(String Tenncc) {
        this.Tenncc = Tenncc;
    }

    /**
     * @return the SDT
     */
    public String getSDT() {
        return SDT;
    }

    /**
     * @param SDT the SDT to set
     */
    public void setSDT(String SDT) {
        this.SDT = SDT;
    }

    /**
     * @return the DC
     */
    public String getDC() {
        return DC;
    }

    /**
     * @param DC the DC to set
     */
    public void setDC(String DC) {
        this.DC = DC;
    }

    /**
     * @return the CMND
     */
    public String getCMND() {
        return CMND;
    }

    /**
     * @param CMND the CMND to set
     */
    public void setCMND(String CMND) {
        this.CMND = CMND;
    }
    @Override
    public String toString() {
        return Mancc + "#" + Tenncc + "#" + SDT + "#" + DC + "#" + CMND;
    }
    public boolean KT( ArrayList<NhaCungCap> nv,String a) throws ParseException
    {
       if(nv==null) return false;
       for(int i=0;i<nv.size();i++)
       {if(nv.get(i).Mancc.toLowerCase().equals(a.toLowerCase()))
       {return true;}}
       return false;
    }
    public void Nhap(ArrayList<NhaCungCap> nv) throws ParseException {
        Scanner nhap = new Scanner(System.in);
        boolean kt;
        do {
            System.out.println("Nhap ma nha cung cap : ");
            Mancc = nhap.nextLine().trim();
            kt=Mancc.trim().equals("")==true||KT(nv,Mancc);   
            if (kt)
                    System.out.println(" Ma nha cung cap da ton tai moi nhap lai!");
                    
        } while (kt);
        do {
            System.out.println("Nhap ten nha cung cap :  ");
            Tenncc = nhap.nextLine();
        } while ("".equals(Tenncc));
         do {
            System.out.print("Nhập Số điện thoại : ");
            SDT = nhap.nextLine();
            if (SDT.matches("[0-9]{10}") == false) {
                System.out.println(" Số điện thoại phải có 10 chữ số và gồm các chữ số 0-9. Hãy nhập lại ");
            }
        } while (SDT.matches("[0-9]{10}") == false);
        do {
            System.out.println("Nhap so chung minh nhan dan:  ");
            CMND = nhap.nextLine();
        } while ("".equals(CMND));
        do {
            System.out.println("Nhap dia chi cua nhan vien :  ");
            DC = nhap.nextLine();
        } while ("".equals(DC));
    }

    public void update() {
        Scanner nhap = new Scanner(System.in);
        do {
            System.out.println("Nhap ten nhan vien :  ");
            Tenncc = nhap.nextLine();
        } while ("".equals(Tenncc));
         do {
            System.out.print("Nhập Số điện thoại : ");
            SDT = nhap.nextLine();
            if (SDT.matches("[0-9]{10}") == false) {
                System.out.println(" Số điện thoại phải có 10 chữ số và gồm các chữ số 0-9. Hãy nhập lại ");
            }
        } while (SDT.matches("[0-9]{10}") == false);

        do {
            System.out.println("Nhap dia chi cua nhan vien :  ");
            DC = nhap.nextLine();
        } while ("".equals(DC));
        do {
            System.out.println("Nhap so chung minh nhan dan:  ");
            CMND = nhap.nextLine();
        } while ("".equals(DC));
    }
    public void Hienthi() {
        System.out.printf("%-10s\t%-20s\t%-10s\t%-30s\t%-9s\n", Mancc, Tenncc, SDT, DC, CMND);

    }

}
