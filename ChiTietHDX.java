/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package newpro;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author My Computer
 */
public class ChiTietHDX {
    public String MaG;
    public int SL;
    public QLSneakers dssp = new QLSneakers();
    public ArrayList<Giay> s;

    /**
     * @return the MaG
     */
    public String getMaG() {
        return MaG;
    }

    /**
     * @param MaG the MaG to set
     */
    public void setMaG(String MaG) {
        this.MaG = MaG;
    }

    /**
     * @return the SL
     */
    public int getSL() {
        return SL;
    }

    /**
     * @param SL the SL to set
     */
    public void setSL(int SL) {
        this.SL = SL;
    }
    public ChiTietHDX() throws ParseException {
        MaG = "";
        SL = 0;
        s = dssp.readds();
    }
    public ChiTietHDX(String MaG, int SL) throws ParseException {
        this.MaG = MaG;
        this.SL = SL;
        s = dssp.readds();
    }
    public ChiTietHDX(String xh) throws ParseException {
        String[] d = xh.split(",");
        MaG = d[0];
        SL = Integer.parseInt(d[1]);
        s = dssp.readds();
    }
    @Override
    public String toString() {
        return MaG + "," + SL;
    }
    // Lấy ra giá của quần áo có mã : ma
      public int GiaG(String ma) {  
        int t=0;
        for (int i = 0; i < s.size(); i++) {
            if (s.get(i).MaG.equals(ma)) 
                t=s.get(i).Gia;
            }
        return t;   
    }
    public String Ten(String ma) {
        String t = null;
        for (int i = 0; i < s.size(); i++) {
            if (s.get(i).MaG.equals(ma)) {
                t = s.get(i).getTenG();
            }
        }
        return t;
    }
    // Kiểm tra mã trùng
    public boolean Trung(String ma) {
        for (int i = 0; i < s.size(); i++) {
            if (s.get(i).MaG.toLowerCase().equals(ma.toLowerCase())) {
                return true;
            }
        }
        return false;
    }
     public void Nhap() {
        Scanner scr = new Scanner(System.in);

        do {
            System.out.print(" Nhap ma giay: ");
            MaG = scr.nextLine();
            if (MaG.trim().equals("") || Trung(MaG)==false) {
                System.out.println("Ma giay trông hoac Khong ton tai san pham co ma nhu vay. Hay nhap lai !");
            }
        } while (MaG.trim().equals("") || Trung(MaG)==false);
        do {
            System.out.print(" So luong: ");
            SL = scr.nextInt();
            if (SL <= 0) {
                System.out.println(" So luong phai lon hon 0: ");
            }
        } while (SL <= 0);
    }
     public int ThanhTien() {
        return  (int) SL*GiaG(MaG);
    }

    public void HienThi() {
        System.out.printf("%-20s\t%-20s\t%-15s\t%-20s\n",MaG,Ten(MaG),SL,ThanhTien());
    }
}
