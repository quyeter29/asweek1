
package newpro;
import java.util.*;
import java.text.*;
import java.io.*;

public class Thongke {

    public Scanner scr = new Scanner(System.in);

   public void DoanhThuThang() {
        Scanner sc = new Scanner(System.in);
        try {
            System.out.print("Nhập tháng/năm kiểm tra: ");
            ngay = tg2.parse(sc.nextLine());
            float t = 0;
            QLHDX dshdx = new QLHDX();
            ArrayList<HoaDonXuat> hd = dshdx.readds();
            for (int i = 0; i < hd.size(); i++) {
                if (hd.get(i).ngayXuat.getYear() == ngay.getYear() && hd.get(i).ngayXuat.getMonth() == ngay.getMonth()) {
                    t = (float) (hd.get(i).tongtien() + t);
                }
            }
            System.out.println(" Doanh thu trong tháng: " + tg2.format(ngay) + "= " + t);
        } catch (ParseException e) {
            System.out.print("Sai định dạng ");
        }
    }
    SimpleDateFormat tg1 = new SimpleDateFormat("dd/MM/yyyy");
    SimpleDateFormat tg2 = new SimpleDateFormat("MM/yyyy");
    public Date ngay;

    public void DoanhThuNgay() {
        Scanner sc = new Scanner(System.in);
        try {
            System.out.print("Nhập ngày/tháng/năm kiểm tra: ");
            ngay = tg1.parse(sc.nextLine());
        float t = 0;
        QLHDX dshdx = new QLHDX();
        ArrayList<HoaDonXuat> hd = dshdx.readds();
        for (int i = 0; i < hd.size(); i++) {
            if (hd.get(i).ngayXuat.equals(ngay)) {

                t = (float) (hd.get(i).tongtien() + t);
            }
        }
        System.out.println(" Doanh thu trong ngày: " + tg1.format(ngay) + "= " + t);
        } catch (ParseException e) {
            System.out.print("Sai định dạng ");
        }
    }

//    public void Giaybanchay(ArrayList<HoaDonXuat> dsach) throws ParseException {
//        QLSneakers giay = new QLSneakers();
//        ArrayList<Giay> dsg = giay.readds();
//        String[] mg = new String[dsg.size()];
//        float[] sl = new float[dsg.size()];
//        for (int i = 0; i < dsg.size(); i++) {
//            mg[i] = dsg.get(i).getMaG();
//            sl[i] = 0;
//        }
//        for (HoaDonXuat hdx : dsach) {
//                for (int i = 0; i < dsg.size(); i++) {
//                    if (hdx.getMaG().equals(mg[i])) {
//                        sl[i] += hdx.getSL();
//                    }
//                }
//            }
//        
//        float max = -1;
//        int cs = -1;
//        for (int i = 0; i < dsg.size(); i++) {
//            if (max < sl[i]) {
//                max = sl[i];
//                cs = i;
//            }
//        }
//        System.out.println("Thong tin giay ban chay nhat :");
//        for (Giay g : dsg) {
//            if (g.getMaG().equals(mg[cs])) {
//                g.Hienthi();
//            }
//        }
//    }

    
    public void menudk3(ArrayList<HoaDonXuat> dsach) throws ParseException, IOException {
//        Thongke chose = new Thongke();
        int chon;
        boolean kt;
        do {
            kt = true;
            chon = -1;
            System.out.println("\n");
            System.out.println("THONG KE ");
            System.out.println("------------------------------");
            System.out.println(" 1.Thong ke doanh thu theo thang ");
            System.out.println(" 2.Thong ke doanh thu theo ngay ");
            System.out.println(" 0.Exit ");
            System.out.println("------------------------------");
            do {
                try {
                    System.out.println(" Chon cong viec can thuc hien ! ");
                    chon = scr.nextInt();
                    if (chon < 0 || chon > 2) {
                        throw new Exception("Nhap sai!Moi nhap lai!");
                    }
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            } while (chon < 0 || chon > 2);

            switch (chon) {
                case 1:
                    DoanhThuThang();
                    break;
                case 2:
                    DoanhThuNgay();
                    break;
                case 0:
                    kt = false;
                    break;
            }
        } while (kt);
    }
}