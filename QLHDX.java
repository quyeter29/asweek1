/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package newpro;
import java.io.*;
import java.text.*;
import java.util.*;
/**
 *
 * @author My Computer
 */
public class QLHDX {
ArrayList<HoaDonXuat> DSach ;
    public QLHDX(){
        try{DSach=readds();
        }catch(Exception ex)
        {
                    System.out.println(ex.getMessage());
        }
        }
    public Scanner sc = new Scanner(System.in);
    public ArrayList<HoaDonXuat> readds() throws ParseException {
        BufferedReader br = null;
        DSach = new ArrayList<>();
       try {
            File file=new File("D:\\DoAn\\hoadonxuat.txt");
            if(!file.exists()){file.createNewFile();}
            br = new BufferedReader(new FileReader("D:\\DoAn\\hoadonxuat.txt"));
            String ttdsb = br.readLine();
            while (ttdsb != null) {
                HoaDonXuat ds = new HoaDonXuat(ttdsb);
                DSach.add(ds);
                ttdsb = br.readLine();
                br.close();//Ghitep(DSach);
            }
        } catch (IOException e) {System.out.println(Arrays.toString(e.getStackTrace()));
}
        return DSach;
    }
    

    public void Ghitep() throws IOException {
        BufferedWriter bw = null;
        try {
            bw = new BufferedWriter(new FileWriter("D:\\DoAn\\hoadonxuat.txt"));
            for (int i = 0; i < DSach.size(); i++) {
                bw.write(DSach.get(i).toString());
                bw.newLine();
            }
        } catch (IOException e) {
        } finally {
            try {
                bw.close();
                System.out.println("Ghi tep thanh cong");
            } catch (IOException e) {
            }
        }
    }
    public void nhapds() throws ParseException {
        do {
            HoaDonXuat ds = new HoaDonXuat();
            ds.Nhap();
            DSach.add(ds);
            System.out.println("Nhan phim bat ky de tiep tuc hoac enter de ket thuc ! ");
            String kt = sc.nextLine();
            if (kt.equals("")) {
                break;
            }
        } while (true);
    }
  //Tim kiem theo ma hoa don
    public HoaDonXuat TKMaHD( String Ma) {
        for (int i = 0; i < DSach.size(); i++) {
            if (DSach.get(i).getMa_HDX().equalsIgnoreCase(Ma)) {
                return DSach.get(i);
            }
        }
        return null;
    }
    public ArrayList<HoaDonXuat> TKTNN( String ten) throws ParseException {
        ArrayList<HoaDonXuat> ds = new ArrayList<>();
        for (int i = 0; i < DSach.size(); i++) {
            if (DSach.get(i).getMa_NV().toLowerCase().contains(ten.toLowerCase())) { //toLowerCase: dua tat ca chu cai ve viet thuong
                ds.add(DSach.get(i));
            }
        }
        return ds;
    }
//Cap nhat
    public void Capnhat() throws ParseException, IOException {
        int cs = -1;
        System.out.println("Nhap ma hoa don can sua : ");
        String mahdn = sc.nextLine();
        for (int i = 0; i < DSach.size(); i++) {
            if (DSach.get(i).getMa_HDX().equals(mahdn)) {
                cs = i;
            }
        }
        if (cs != -1) {
            DSach.get(cs).update();
        } else {
            System.out.println("Khong ton tai ma hoa don nay ! ");
        }
    }
    
    public void XoaHDX(ArrayList<HoaDonXuat> dshd)  throws IOException {
        Scanner scr = new Scanner(System.in);
        int tg = -1;
        String ma = "";
        do {
            System.out.print("Nhập Mã hóa đơn xuất cần xóa hoặc Enter để thoát");
            ma = scr.nextLine();
            for (int i = 0; i < dshd.size(); i++) {
                if (dshd.get(i).getMa_HDX().equals(ma)) {
                    dshd.remove(i);
                    tg = 1;
                    break;
                }
            }
            if (ma.equals("") == false) {
                if (tg == (-1)) {
                    System.out.println("Không tìm thấy Thông tin hóa đơn nhập có mã là: " + ma);
                } else {
                    System.out.println("Đã xóa Thông tin hóa đơn nhập có mã là: " + ma);
                }
            }
        } while (ma.equals("") == false);
        Ghitep();
    }
    public void hienthids(ArrayList<HoaDonXuat> dsg) {
        for (int i = 0; i < dsg.size(); i++) {
//            HoaDonXuat a=new HoaDonXuat();
//            System.out.println(a.ma_NV);
            dsg.get(i).Hien();
            System.out.println("Tong tien: " + dsg.get(i).tongtien());
            System.out.println();
        }
    }
//    public void hienthids(ArrayList<HoaDonXuat> dsg) {
//        System.out.println("Danh sach thong tin chi tiet hoa don :");
//        System.out.printf("\n%-5s\t%-10s\t%-10s\t%-8s\t%-10s\t%-9s\t%-6s\t%-2s\t%-8s\t%-8s\n", "MaHD", "MaG", "TenG", "Loaigiay", "Nguoinhap", "Ngaynhap", " Kichco", "SL", "DG", "TT");
//        for (int i = 0; i < dsg.size(); i++) {
//            dsg.get(i).Hien();
//        }
//    }
    public void menusearch() throws ParseException, IOException {
        Scanner scr = new Scanner(System.in);
        QLHDX chose = new QLHDX();
        String chon;
        do {
            System.out.println("\n");
            System.out.println("------------------------------");
            System.out.println(" Chon phuong thuc tim kiem! ");
            System.out.println(" 1.Tim kiem theo ma hoa don ");
            System.out.println(" 2.Tim kiem theo ma nhan vien ");
//          System.out.println(" 3.Tim kiem theo ten nhan vien ");
//            System.out.println(" 4.Tim kiem nguoi nhap");
            System.out.println(" 0.Exit ");
            chon = scr.nextLine();
            switch (chon) {
                 case "1":
                    System.out.println("Nhap ma hoa don can tim ");
                    String Ma = scr.nextLine();
                    HoaDonXuat ds = chose.TKMaHD(Ma);
                    if (ds == null) {
                        System.out.println("Ma hoa don nay khong ton tai! ");
                    } else {
                        ds.Hien();
                    }
                    break;
//                case "2":
////                    System.out.println("Nhap ngay can tim ");
////                    String ten = scr.nextLine();
//                    HoaDonXuat dsgi = chose.TKTNN(ten );
//                    if (dsgi == null) {
//                        System.out.println("Ngay nhap khong ton tai! ");
//                    } else {
//                        dsgi.Hien();
//                    }
//                    break;
//                case "3":
//                    System.out.println("Nhap ten giay can tim ");
//                    String ten = scr.nextLine();
//                    ArrayList<HoaDonXuat> dsg = chose.TKTenG( ten);
//                    if (dsg.isEmpty()) {
//                        System.out.println("Giay nay khong ton tai! ");
//                    } else {
//                        chose.hienthids(dsg);
//                    }
//                    break;

                case "2":
                    System.out.println("Nhap vao ma nhan vien can tim");
                    String ten = scr.nextLine();
                    ArrayList<HoaDonXuat> kc = chose.TKTNN( ten);
                    if (kc.isEmpty()) {
                        System.out.println("Khong ton tai nguoi nay");
                    } else {
                        chose.hienthids(kc);
                    }
                case "0":
                        menu n=new menu();n.menuchinh();
                    //chose.menudk4();
                    break;
            }
        } while (true);
    }

    public void menudk4() throws ParseException, IOException {
        Scanner scr = new Scanner(System.in);
        HoaDonXuat chose = new HoaDonXuat();
        String chon;
        boolean kt;
        do {
            chon = null;
            kt = true;
            System.out.println("\n");
            System.out.println("QUAN LY DANH SACH CHI TIET HOA DON XUAT");
            System.out.println("------------------------------");
            System.out.println(" 1.Nhap danh sach chi tiet hoa don ");
            System.out.println(" 2.Hien thi danh sach chi tiet hoa don ");
            System.out.println(" 3.Cap nhat hoa don ");
            System.out.println(" 4.Tim kiem ");
            System.out.println(" 5.Xoa ");
            System.out.println(" 0.Exit ");
            System.out.println("------------------------------");
            do {
                try {
                    System.out.println(" Chon cong viec can thuc hien ! ");
                    chon = scr.nextLine();
                    if (chon.compareTo("0") == -1 || chon.compareTo("5") == 1) {
                        throw new Exception("");
                    }
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            } while (chon.compareTo("0") == -1 || chon.compareTo("5") == 1);

            switch (chon) {
                case "1":
                    nhapds();
                    Ghitep();
                    break;
                case "2":
                    hienthids(DSach);
                    break;
                case "3":
                    Capnhat();
                    Ghitep();
                    break;

                case "4":
                    menusearch();
                    break;
                case "5":
                    XoaHDX(DSach);
                    break;
                case "0":
                    kt = false;
                    break;
            }
        } while (kt);
    }
}

